FROM docker.io/rhel7
MAINTAINER Nicolas Mallet <nicolas.mallet@jcdecaux.com>

COPY jcdecaux.repo /etc/yum.repos.d/jcdecaux.repo
COPY RPM-GPG-KEY-EPEL-7 /etc/pki/rpm-gpg/RPM-GPG-KEY-EPEL-7

RUN yum update -y && yum clean all
